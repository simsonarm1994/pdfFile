import PDFJS from 'pdfjs-dist';
import React, { Component } from 'react';

function gettext(pdfUrl){
    let pdf = PDFJS.getDocument(pdfUrl);
    return pdf.then(function(pdf) { // get all pages text
        let maxPages = pdf.pdfInfo.numPages;
        let countPromises = []; // collecting all page promises
        for (let j = 1; j <= maxPages; j++) {
            let page = pdf.getPage(j);
            console.log(j);

            let txt = "";
            countPromises.push(page.then(function(page) { // add page promise
                let textContent = page.getTextContent();
                return textContent.then(function(text){ // return content promise
                    // console.log(j);
                    // console.log(text);
                    return text.items.map(function (s) {

                        return s.str;
                    }).join(''); // value page text

                });
            }));
        }
        // Wait for all pages and join text
        return Promise.all(countPromises).then(function (texts) {
            console.log(texts);
            return texts.join('');
        });
    });
}

class App extends Component {
    constructor(props){
        super(props);
        this.state={
            text: null
        }
    }

    componentWillMount(){
        gettext("https://cdn.mozilla.net/pdfjs/tracemonkey.pdf").then(function (text) {
            this.setState({text:text})
        }.bind(this), function (reason) {
            console.error(reason);
        });

    }
    render() {

    return (
        <div>
            {this.state.text ? this.state.text : "...loading"}
        </div>
    )
  }
}

export default App;